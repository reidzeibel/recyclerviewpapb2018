package papb2018.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class MahasiswaHolder extends RecyclerView.ViewHolder {

    TextView nama;
    TextView nim;
    TextView prodi;

    public MahasiswaHolder(View itemView) {
        super(itemView);

        nama = itemView.findViewById(R.id.nama);
        nim = itemView.findViewById(R.id.nim);
        prodi = itemView.findViewById(R.id.prodi);
    }

}

package papb2018.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class MahasiswaAdapter extends
        RecyclerView.Adapter<MahasiswaHolder> {

    //buat sebuah variabel/field untuk menampung daftar mahasiswa
    List<Mahasiswa> daftarMahasiswa;

    //constructor
    public MahasiswaAdapter(List<Mahasiswa> daftarMahasiswa) {
        this.daftarMahasiswa = daftarMahasiswa;
    }

    @NonNull
    @Override
    public MahasiswaHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Inflate View menggunakan layoutInflater
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.item_mahasiswa, viewGroup, false);
        // View untuk layout item_mahasiswa sudah siap
        // buat MahasiswaHolder menggunakan View yang kita inisialisasi
        return new MahasiswaHolder(view);
    }

    @Override
    public void onBindViewHolder(MahasiswaHolder mahasiswaHolder, int i) {
        //ambil sebuah item mahasiswa pada posisi "position"
        Mahasiswa mahasiswa = daftarMahasiswa.get(i);

        mahasiswaHolder.nama.setText(mahasiswa.nama);
        mahasiswaHolder.nim.setText(mahasiswa.nim);
        mahasiswaHolder.prodi.setText(mahasiswa.prodi);

    }

    @Override
    public int getItemCount() {
        return daftarMahasiswa.size();
    }
}

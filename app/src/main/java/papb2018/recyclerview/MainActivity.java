package papb2018.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Mahasiswa ridwan = new Mahasiswa(
                "Ridwan Aditama",
                "11770",
                "Ilmu Komputer");

        Mahasiswa faris = new Mahasiswa(
                "Faris",
                "11798",
                "Ilmu Komputer");

        Mahasiswa alvy = new Mahasiswa(
                "Alvy",
                "11769",
                "Ilmu Komputer");

        Mahasiswa ali = new Mahasiswa(
                "Ali",
                "11768",
                "Ilmu Komputer");

        //Buat sebuah list kosong dan inisialisasi list tersebut
        List<Mahasiswa> listMahasiswa =       // List dengan nama listMahasiswa
                new ArrayList<Mahasiswa>();   // inisialisasi dengan tipe list "ArrayList"
              //new Mahasiswa(); <--- mirip seperti ini, sebuah objek kosong

        listMahasiswa.add(ridwan);
        listMahasiswa.add(faris);
        listMahasiswa.add(alvy);
        listMahasiswa.add(ali);

        // inisialisasi RecyclerView (samakan ID dengan id recyclerView
        // di activity_main.xml
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        // inisialisasi Adapter
        MahasiswaAdapter adapter = new MahasiswaAdapter(listMahasiswa);

        // sambungkan RecyclerView dengan Adapter
        recyclerView.setAdapter(adapter);

        // tambahkan layoutmanager untuk RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}

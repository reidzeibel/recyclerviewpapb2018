package papb2018.recyclerview;

public class Mahasiswa {

    String nama;
    String nim;
    String prodi;

    public Mahasiswa() {

    }

    //Constructor (tekan alt+insert, pilih Constructor)
    public Mahasiswa(String nama, String nim, String prodi) {
        this.nama = nama;
        this.nim = nim;
        this.prodi = prodi;
    }
}
